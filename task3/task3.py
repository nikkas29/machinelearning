import pandas as pd
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import sklearn.linear_model as linear_model

data = pd.read_csv("data/train.csv")
dateTime = pd.to_datetime(data["datetime"])
data["hour"] = dateTime.map(lambda x: x.hour)
data["weekday"] = dateTime.map(lambda x: x.weekday())
data["month"] = dateTime.map(lambda x: x.month)
data["year"] = dateTime.map(lambda x: x.year)


data = data.drop(['datetime'], axis=1)
corr = data.corr()
fig, ax = plt.subplots(figsize=(12, 12))
ax.matshow(corr)
plt.xticks(range(len(corr.columns)), corr.columns)
plt.yticks(range(len(corr.columns)), corr.columns)
plt.show()

X = data.drop(['registered', 'count'], axis=1)
Y_one_hot = data['count']

X_train, X_test, Y_train, Y_one_hot_test = train_test_split(X, Y_one_hot, test_size=0.2)

scaler = StandardScaler()
scaler.fit(X_train, Y_train)
X_scaled = scaler.transform(X_train)
X_scaled_test = scaler.transform(X_test)

regrsgd = linear_model.SGDRegressor()
regrsgd.fit(X_scaled, Y_train)
pred = regrsgd.predict(X_scaled_test)

print 'mean squared error with categorical feature: ' + str(metrics.mean_squared_error(Y_one_hot_test, pred))

# mean squared error with categorical feature: 12968.89678015906

# ----------------------------------------------------------------------------------------------------

X_train_without_categorical = X_train.drop(['season', 'holiday', 'workingday', 'weather', 'hour'], axis=1)
X_test_without_categorical = X_test.drop(['season', 'holiday', 'workingday', 'weather', 'hour'], axis=1)

scaler.fit(X_train_without_categorical, Y_train)
X_scaled = scaler.transform(X_train_without_categorical)
X_scaled_test = scaler.transform(X_test_without_categorical)

regrsgd.fit(X_scaled, Y_train)
pred = regrsgd.predict(X_scaled_test)

print'mean squared error without categorical feature: ' + str(metrics.mean_squared_error(Y_one_hot_test, pred))

# mse score without categorical feature: 14869.349926101117

# ----------------------------------------------------------------------------------------------------

print (data.drop(['registered', 'count'], axis=1).columns.values)
one_hot_encoder = OneHotEncoder(sparse=False,
                                categorical_features=[0, 1, 2, 3, 9, 10, 11, 12])

data_one_hot_encoded = one_hot_encoder.fit_transform(data.drop(['registered', 'count'], axis=1))

X_one_hot, X_one_hot_test, Y_one_hot, Y_one_hot_test = train_test_split(
    data_one_hot_encoded, data['count'],
    test_size=0.2, random_state=11)

scaler.fit(X_one_hot, Y_one_hot)
X_one_hot_scaled = scaler.transform(X_one_hot)
X_one_hot_scaled_test = scaler.transform(X_one_hot_test)

regrsgd.fit(X_one_hot_scaled, Y_one_hot)
pred = regrsgd.predict(X_one_hot_scaled_test)

print 'mean squared error one_hot_encoded: ' + str(metrics.mean_squared_error(Y_one_hot_test, pred))

# mean squared error one_hot_encoded: 10844.393966555475

# ----------------------------------------------------------------------------------------------------

print (data.drop(['atemp', 'casual', 'registered', 'count'], axis=1).columns.values)
one_hot_encoder = OneHotEncoder(sparse=False, categorical_features=[0, 1, 2, 3, 7, 8, 9, 10])

data_one_hot_encoded = one_hot_encoder.fit_transform(data.drop(['atemp', 'casual', 'registered', 'count'], axis=1))

X_one_hot, X_one_hot_test, Y_one_hot, Y_one_hot_test = train_test_split(
    data_one_hot_encoded, data['count'],
    test_size=0.2, random_state=11)

scaler.fit(X_one_hot, Y_one_hot)
X_one_hot_scaled = scaler.transform(X_one_hot)
X_one_hot_scaled_test = scaler.transform(X_one_hot_test)

regrsgd.fit(X_one_hot_scaled, Y_one_hot)
pred = regrsgd.predict(X_one_hot_scaled_test)

print 'mean squared error (one_hot_encoded -casual & -atemp): ' + str(metrics.mean_squared_error(Y_one_hot_test, pred))

# mean squared error (one_hot_encoded -casual & -atemp): 7294.031283459636

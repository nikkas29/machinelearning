import csv
import random
import numpy as np
from sklearn.svm import SVR
from sklearn.metrics import explained_variance_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

NUM_ENTRIES = [0, 1, 4, 5, 6, 7, 8, 9, 10, 11]
MONTH = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
DAY = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
TRAIN_SIZE = 0.8
COLUMNS_COUNT = 29

with open('forestfires.csv', 'rb') as f:
    reader = csv.reader(f)
    raw_data = list(reader)
raw_data = raw_data[1:]
area = np.array(raw_data)[:, 12]

data = list()
for entry in raw_data:
    mod_entry = []
    for i in range(len(entry)):
        if i == 12:
            mod_entry.append(np.log(float(entry[i]) + 1))
        elif i in NUM_ENTRIES:
            mod_entry.append(float(entry[i]))
        elif i == 2:
            zero = np.zeros(12)
            zero[MONTH.index(entry[i])] = 1
            for j in zero:
                mod_entry.append(j)
        else:
            zero = np.zeros(7)
            zero[DAY.index(entry[i])] = 1
            for j in zero:
                mod_entry.append(j)
    data.append(mod_entry)

random.shuffle(data)
data = np.array(data)

X_train = data[0:int(TRAIN_SIZE * len(data)), :COLUMNS_COUNT]
Y_train = data[0:int(TRAIN_SIZE * len(data)), COLUMNS_COUNT]
X_test = data[int(TRAIN_SIZE * len(data)):, 0:COLUMNS_COUNT]
Y_test = data[int(TRAIN_SIZE * len(data)):, COLUMNS_COUNT]

svr_rbf = SVR(kernel='rbf', C=3, gamma=1, epsilon=0.01)
svm_model = svr_rbf.fit(X_train, Y_train)

pred = svm_model.predict(X_test)

score = explained_variance_score(Y_test, pred)
mse = mean_squared_error(Y_test, pred)
mae = mean_absolute_error(Y_test, pred)

print("Explained variance score", explained_variance_score(Y_test, pred))
print("Mean squared error", mean_squared_error(Y_test, pred))
print("Mean absolute error", mean_absolute_error(Y_test, pred))

# ('Explained variance score', '0.05591409855116003')
# ('Mean squared error', '1.416567583850026')
# ('Mean absolute error', '1.0114631413838637')
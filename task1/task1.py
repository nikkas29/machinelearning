import numpy
import matplotlib
import matplotlib.pyplot as pyplot
from sklearn.metrics import f1_score

from sklearn import neighbors, model_selection
from sklearn.model_selection import GridSearchCV

def save(fname, data, width, height):
    image = numpy.array(data).reshape(width, height)
    matplotlib.pyplot.imsave(fname, image, cmap=matplotlib.cm.gray)


file = open("semeion.data.txt", 'r')
lines = file.readlines()

width = 16
height = 16
size = width * height
classes = 10

images = []
labels = []
fnumber = 0


def whatValue(turp):
    result = 0
    for i in range(10):
        result += turp[i] * i
    return result


for line in lines:
    data = line.split(' ')
    image = []
    label = []

    for i in range(0, size):
        image.append(int(float(data[i])))
    images.append(image)

    for i in range(size, size + classes):
        label.append(int(float(data[i])))
    labels.append(whatValue(label))

    # save(".\images\\" + str(fnumber) + '.png', image, width, height)
    fnumber += 1


images_train, images_test, labels_train, labels_test = model_selection.train_test_split(images, labels, test_size=0.2)
knn = neighbors.KNeighborsClassifier(n_neighbors=5)
knn.fit(images_train, labels_train)
pred = knn.predict(images_test)

f1 = f1_score(labels_test, pred, average='micro')

print 'f1 score: ' + str(f1)

# f1 score: 0.9042386185243328

cv = GridSearchCV(knn,
                  {'n_neighbors': list(range(4, 20)),
                   'metric': ['minkowski', 'euclidean', 'manhattan', "cosine"],
                   'weights': ['uniform', 'distance']},
                  scoring='f1_micro', cv=3, n_jobs=1)
cv.fit(images_train, labels_train)

print("best params: " + str(cv.best_params_))
print("best score: " + str(cv.best_score_))

# results:
# best params: {'n_neighbors': 5, 'metric': 'manhattan', 'weights': 'distance'}
# best score: 0.9090909090909091
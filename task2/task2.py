import pandas as pd
import numpy as np
from sklearn import tree, model_selection
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import graphviz


# def categorical_to_numeric(x):
#     if 0 <= x < 6:
#         return 0
#     elif 6 <= x < 13:
#         return 1
#     elif 13 <= x < 19:
#         return 2
#     elif 19 <= x < 24:
#         return 3


data = pd.read_csv("data/train.csv")
dateTime = pd.to_datetime(data["datetime"])
data["hour"] = dateTime.map(lambda x: x.hour)
data["month"] = dateTime.map(lambda x: x.month)
data["weekday"] = dateTime.map(lambda x: x.weekday())
# data['hour'] = data['hour'].apply(categorical_to_numeric)
data = data.drop(['datetime'], axis=1)

corr = data.corr()
fig, ax = plt.subplots(figsize=(12, 12))
ax.matshow(corr)
plt.xticks(range(len(corr.columns)), corr.columns)
plt.yticks(range(len(corr.columns)), corr.columns)
plt.show()

X = data.drop(['registered', 'count'], axis=1)
Y = data['count']

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)

DecTree = DecisionTreeRegressor()
DecTree.fit(X_train.values, Y_train.values)
pred = DecTree.predict(X_test)

rounder = lambda t: int(t)
vfunc = np.vectorize(rounder)

print(metrics.mean_squared_error(vfunc(pred), Y_test.values))

# 4830.203856749311

dot_data = tree.export_graphviz(DecTree, out_file=None,
                                filled=True, rounded=True,
                                special_characters=True,
                                feature_names=X.columns,
                                max_depth=4)
graph = graphviz.Source(dot_data)
graph.view()

param_grid = {'max_depth': np.arange(3, 10),
              'criterion': ["mse", "mae"],
              'min_samples_leaf': np.arange(1, 5),
              'min_samples_split': np.arange(2, 5)
              }
cv = model_selection.GridSearchCV(DecTree, param_grid, scoring='neg_mean_squared_error', cv=4)
cv.fit(X_train, Y_train)

print("params: " + str(cv.best_params_))
print("score: " + str(-cv.best_score_))

DecTree.set_params(**cv.best_params_)
DecTree.fit(X, Y)

dot_data = tree.export_graphviz(DecTree, out_file=None,
                                filled=True, rounded=True,
                                special_characters=True,
                                feature_names=X.columns,
                                max_depth=4)
graph = graphviz.Source(dot_data)
graph.view()

# params: {'min_samples_split': 4, 'criterion': 'mse', 'max_depth': 9, 'min_samples_leaf': 4}
# score: 3805.1242989704583